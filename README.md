# \<computer-cnmd\>



## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your element locally.

## Clone Repository
you might make a new folder, where you must clone this repository:
```
$git clone git@gitlab.com:CristianMH92/computer-cnmd.git
```
## Install Dependences

```
$ bower install
```
## Viewing Your Element
In your main folder you must doing: 
```
$ polymer serve
```

## Running Tests

```
$ polymer test
```
## Author
```
@CristianMH92 GitLab
cristian92mar@gmail.com
d(n.n)b
```
Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.

